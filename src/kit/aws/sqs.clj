(ns kit.aws.sqs
  (:require
   [camel-snake-kebab.core :as csk]
   [kit.aws.core :as ac]))

(defonce client (atom nil))

(defn init!
  ([]
   (init! {}))
  ([conf]
   (reset! client (ac/make-client
                   (merge {:api :sqs} conf)))))

;; (init! {})

(defn list-queues []
  (:queue-urls (ac/invoke @client :list-queues)))

;; (list-queues)

(defn get-queue-attributes
  ([queue-url]
   (get-queue-attributes queue-url [:all]))
  ([queue-url attribute-names]
   (ac/invoke @client
              :GetQueueAttributes
              {:queue-url queue-url
               :attribute-names (map csk/->PascalCaseString attribute-names)})))

(defn send-message
  ([queue-url message-body]
   (send-message queue-url message-body nil))
  ([queue-url
    message-body
    {:as request
     :keys [delay-seconds
            message-attributes
            message-system-attributes
            message-deduplication-id
            message-group-id]}]
   (ac/invoke @client
              :send-message
              (merge {:queue-url queue-url
                      :message-body message-body}
                     request))))

(defn purge-queue [queue-url]
  (ac/invoke @client :purge-queue {:queue-url queue-url}))
